---
title: "Modularity vs. Simplicity: Building a Personal Site"
tags: [mithril]
---

I built my personal site here during what amounted to a personal hackathon, one day. It was one really one of my first completed, useful projects, as a student.

I had a lot of projects going on at the time. While I went back and polished it _just_ enough to make it presentable, and often got compliments on it, it always irritated me:

-I knew it wouldn't work on screens larger than 1080p.
-I knew I had committed build files to the repo, since I was still new to those at the time.
-I was pained, later, that bower was shown there, as that had fallen completely out of favor.
-I knew that the HTML files relied on copy/pasted text.
-I knew I was using jquery, entirely unnecessarily.
-Even worse, I knew I wasn't using CDNs for jquery and bootstrap.

It languished.

Now, years later, it's interesting to go back and face it. How do I move forward from here?

It still looks _acceptably_ attractive. It needs to be fleshed out, though. My original goal, having a portfolio there, has not yet come to fruition, and frankly the site provides almost no useful information. It's almost entirely eye-candy and hosts a link to my resume.

After much deliberation, I decided to use Mithril. Mithril can be used like React, as a piece in a complex build chain with babel and webpack, and they provide a walkthrough to do so.  

I hadn't used Mithril in a few years, since before its 1.0 release (when making [mindseal](http://mindseal.kylebaker.io), and I loved it back then. I was looking forward to re-learning the new version.

After following their instructions though, building the increasingly complex tool chain (live reloading, babel transpilation, webpack bundling...), I watched my dependencies grow north of 75mb and felt that familiar JS-cringe. All of this, to support my little homepage? Things didn't work smoothly, in the end, though. Webpack had changed its format for consuming config files. Stackoverflow showed an answer that seemed to fix the immediate error, but that only led to a new silent problem. The more time I spent on this, on the _pre_-code, the more ridiculous it felt.

For a moment, reflecting while on a walk the next day, I decided I was going to just hand-roll a little minimalist SPA framework. But while chewing on the idea in my spare time, I realized that wasn't quite the right approach either.

The nice thing about Mithril is they're quite adamant about keeping the project lightweight in every way--including in regards to build requirements. They keep the codebase in ECMA 5.1, eschewing the need for transpilers. Their tests are in vanilla javascript. And so the 'heavyweight' approach isn't necessary. It's possible to both

===

modularity, maintainability, simplicity, speed, reliability, size.

make an outline.
