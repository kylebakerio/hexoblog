---
title: 'Mixing Hobbies: A BJJ PWA :)'
tags:
---

pdf -> hmm, why isn't this an app?

on vacation in big bend, can't wait to start

(picture of big bend?)

ask him how the data is formatted? He says he can put a spreadsheet together.

car ride home, start in the passenger seat on the long ride home

spreadsheet is problematic--there are more than txo axes worth of data inconsistently layered. Phrases are on rows, but so are topic headers, blank lines, notes. Definition fields at the intersection of a phrase row and a language column sometimes contain multiple definitions, sometimes contain a definition followed by a pronounciation, japanese contains a third layer of _hiragana/katakana_ (sometimes), and the line breaks or slashes or use of the word "or" used to break these up are inconsistent. 

plan is to build out a proof of concept hackathon-style, use info gained to plan further

find some libraries for importing spreadsheets, decide on tabletop.js

need to write a decent amount of code to massage the data, but it's justified since that same code will be able to be recycled to import later into a proper database. The other issue is working through how to model the relationships in the data within the client. Again, this is worthwhile, since I'll be needing to build out a DB to import this to soon anyways.

Main method: add a meta column where I include a tag to let my code now how to parse every line, add a meta row that indicates special characteristics of those languages that have multiple values, and then add text processing to catch as many of the exceptions as I can, and log unusual cases otherwise, which I sometimes hand-fix in the spreadsheet, and sometimes would add handling for in the code.

first version is just an empty view that outputs formatted dictionary object to the console, where I interact with it there, and get logs about the data formatting and importing. Once that is stable enough to move forward, time to build out a quick minimal gui.

grab mithril 1.0, which I've been hankering to use and is perfect here. borrow some components I'd written in previous projects, mix in some pieces from their tutorial, mix in a new html5 feature I hadn't seen before (datalist)

starts coming together. refactor a bit, as the code is pretty ugly.

deploy as soon as it more-or-less works.

tweak for another 24 hours, add fonts for kroean support, have more nuanced gui interaction so changing the input language or topic clears the phrase-input field, and just clicking the phrase-input field clears it so you get access to the datalist and don't have to manually backspace to clear out, etc.

Phase 1 is complete!

(codepen here? showing code and working demo at this stage? April 25 2018)
(alternative: have two mini recorded videos interacting at an early stage)

What's next?

I send him an email:

> 
Here's the process:
1. continuing to clean up the last bits of the data and detail out a model for the data into a database -- japanese and sinhalese seem to be pretty quirky, need to deal with those, etc.
2. fun part: deciding on features for v1, roadmap for adding features in the future, mocking up designs for the interface
3. doing a proper migration to a database. we'll probably be using google cloud's free tier, which should be perfect for this project--1GB of space is likely far more than enough for us, and it'll be very high speed and reliability, and the server demands will be very low, so the f1-micro should do just fine while giving low latency responses.
4. tech-decisions need to be made, what frameworks and languages and deploy process and so on I'll be using. Part of the fun and utility here for me is getting to experiment with some cutting-edge new technologies, so I'll planning to make this a somewhat over-engineered fun portfolio piece to show off, since it's something that has a reasonable chance of seeing real-world use and has a small enough scope to see out to a high-quality "complete" state. The result will be something stable, reliable, offline functional, and able to be contributed to by others.
6. make the damn thing
7. test, fix, test, fix
8. release
9. see what people want, adjust the roadmap, build some more stuff.

>(end quote)


