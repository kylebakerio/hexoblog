---
title: "Why We Hold Ourselves Back: Programming as Therapy"
tags: 
  - psychology
date: 2018-03-05 00:01:26
---

## Origin

It was a few years ago now that I bought this domain, and thought: "I'd like to have a blog on a subdomain of this url. That'd be nice."

I was in school. I finished an assignment early, and knew I'd be looking for jobs soon.

{% img /images/mks-superman.png 300 300 A picture of me in Hack Reactor %}

So I used my extra day to throw together the [first draft of kylebaker.io](http://old.kylebaker.io). I called the repo "portfolio", intending for it to eventually host a curated, animated, interactive display of some projects of mine.

I got as far as getting the host domain, establishing a look I liked, finding that perfect banner, adding some light, classy animation. The site had almost no content, but it had three nearly-empty, almost identical pages.

<video src="/images/kylebaker.io.early.webm" autoplay loop poster="/images/kylebaker.io.early.jpg"></video>

It sat like that for years.

My skill at coding quickly improved, as is the case for beginners, and I cringed to think about someone opening up the console and seeing my crude code from back then. Behind the scenes, I knew it was a rapidly thrown together bloated mess with jquery, bootstrap, a very early version of the `typed.js` library, and mostly boilerplate.

I never got around to adding the project pages. Never got around to adding that blog. 

I would get compliments on it, though. It worked. It was pretty... We're always our own worst critic.

But why did I stop? Why did I hesitate to take the next steps? I would have said I never found the time, but that simply isn't true. We all waste time constantly--I'm no exception.

In reality, I think I was avoiding it.

## Why?

Back then, the idea of installing and running some server-side open-source blog software was just a small bit intimidating. So was the idea of figuring out subdomains, which I'd never set up before (everything around deployment and devops seemed a bit too magical). Like so many things, I knew they weren't hard, and were well within my capability--but there was a subtle, lurking hesitation that kept me from just diving in.

**This wasn't just in programming**. As a child, the implicit message many of us encounter is that finding something easy shows we are smart, and valuable. The corrolary is that struggle is a sign of weakness.

Unfortunately, it's actually the opposite that that's true: struggle is the key to developing yourself.

Programming can be a source of therapy for this problem, if we let it be. The therapy is to start facing one's shortcomings through struggle.

Any decent programmer faces new challenges all day. To let go of the need to prove ourselves, we become humble. Humility grants a freedom to just be yourself.

It's a process, still. It's a regular desensitization.

And over time, the lesson you learn is that the best programmers aren't the lazy naturals--they're the hard working naturals, the ones who aren't afraid to struggle.

Failure is this lesson you encounter in every domain in life, once you open yourself to learning from it. It's a constant source of education. In Go, in Judo, in BJJ, in programming, in relationships. To embrace it and learn from it, rather than reject it--**to see in your failure the perfectly acceptable shortcomings that can be _developed_ and _grown_ and _improved_, and not inalienable, unalterable limitations.**

This is one of life's journeys. I think it's something a lot of intelligent kids were cursed with, and I think it's something a lot of programmers deal with.

A few years later, programming has brought me greater confidence, but it has also brought me greater humility. I'm no longer afraid to throw myself into whatever I need to--not just because I'm confident I can handle it, but because I'm not afraid to mess up along the way.

Back to work. I finally have a blog up here. I just finished removing jquery, reducing it down to one page, cleaning up the CSS, removing the boilerplate, redoing the HTML, removing bootstrap, refactoring the JS. Finally, it's code I'm proud of.

Next up? I'm chewing on that portfolio project again.
