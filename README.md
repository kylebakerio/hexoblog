-look at configuration options more: https://hexo.io/docs/configuration.html
-look at cool stuff: https://github.com/hexojs/awesome-hexo#showcases

to make new post:

`hexo new draft "My Blog Post"`

to run local browsersync'd version:

`hexo server --drafts` (optional: -o opens browser)

to "publish" a single post (moves it from `_drafts` to `_posts`, adds timestamp:

`hexo publish My-First-Blog-Post`

to actually deploy changes to the web:

instead of `hexo deploy`, which is pretty opinionated, I've set up 

`npm run-script deploy`. This runs `clean`, `generate`, and then `surge` with appropriate options.

-------------

Other stuff you shouldn't need anymore:

to make a static version:

`hexo generate`

then, to publish to the world:

`surge public code.blog.kylebaker.io`

if there's anything that doesn't transfer (theme customizations), delete public/ and re-run `hexo generate`

instead of `hexo deploy`, which is pretty opinionated, I've set up `npm run-script deploy`. This runs `clean`, `generate`, and then `surge` with appropriate options.

-settings are in `config.yml`--there's one in root, and one in theme folder.
-there are also some color stuff in the theme folder as .styl files, that's where I set my accent color for instance.
-project descriptions and links are located in `source/_data` file. That header is generated if that file and folder are present.
